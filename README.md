
# AESSC

AESSC (Auto-Encoder for Semi-Supervised Classification) is a classification model able to use labelled and unlabelled data

( README TEMPORAIRE )


## Features

- Light/dark mode toggle
- Live previews
- Fullscreen mode
- Cross platform


## Installation

Install my-project with npm

```bash
  npm install my-project
  cd my-project
```
    
## Documentation

[Documentation](https://linktodocumentation)


## Usage/Examples

```javascript
import Component from 'my-project'

function App() {
  return <Component />
}
```


## Running Tests

To run tests, run the following command

```bash
  npm run test
```


## Authors

- [@Erwangf](https://www.github.com/Erwangf)

